CREATE DATABASE IF NOT EXISTS branch;

USE branch;

CREATE TABLE IF NOT EXISTS Book (
   id INT Primary Key,
   title VARCHAR(100) NOT NULL,
   isbn VARCHAR(15) NOT NULL,
   num_copies INT NOT NULL,
   author_id INT NOT NULL,
   publisher_id INT NOT NULL
);

 CREATE TABLE IF NOT EXISTS Author (
   id INT Primary Key,
   name VARCHAR(30) NOT NULL
);

CREATE TABLE IF NOT EXISTS Publisher (
   id INT Primary Key,
   name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Branch (
 id INT Primary Key,
 publisher_id INT NOT NULL,
 branch_addr VARCHAR(100) NOT NULL
);