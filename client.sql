CREATE DATABASE IF NOT EXISTS client;

USE client;

CREATE TABLE IF NOT EXISTS Contract(
  id INT Primary Key AUTO_INCREMENT,
  estimated_cost DOUBLE NOT NULL,
  completion_date DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS Client(
  id INT Primary Key AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  location VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS Manager(
  id INT Primary Key AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  location VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS Staff(
  id INT Primary Key AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  location VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS Client_Contracts(
  client_id INT NOT NULL,
  contract_id INT NOT NULL
 );

CREATE TABLE IF NOT EXISTS Manager_Contracts(
  manager_id INT NOT NULL,
  contract_id INT NOT NULL
);

CREATE TABLE IF NOT EXISTS Staff_Contracts(
  staff_id INT NOT NULL,
  contract_id INT NOT NULL
);